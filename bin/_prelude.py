import sys
import os

path = os.path.abspath(sys.argv[0])  # normalized path of this program
while os.path.dirname(path) != path:  
    if os.path.exists(os.path.join(path, 'mamaslemonpy', '__init__.py')):
        sys.path.insert(0, path)
        break
    path = os.path.dirname(path)  # move up in hierarchy
