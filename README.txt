# amplikyzer

*amplikyzer* is short for amplicon analyzer.
It is a software to analyze sequenced amplicons with emphasis on
flowgram analysis (resulting from 454 or IonTorrent sequencing) and methylation analysis after sodium bisulfite treatment.

Please read the information in the [Wiki](https://bitbucket.org/svenrahmann/amplikyzer/wiki/Home).

